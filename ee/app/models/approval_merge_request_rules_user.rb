# frozen_string_literal: true

# Model for join table between ApprovalMergeRequestRule and User
class ApprovalMergeRequestRulesUser < ApplicationRecord # rubocop:disable Gitlab/NamespacedClass -- Conventional name for a join class
end
